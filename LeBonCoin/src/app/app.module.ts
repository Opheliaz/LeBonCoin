//Modules
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from "@angular/common/http";
import { RouterModule, Routes }from "@angular/router";


//Components
import { AppComponent } from './app.component';
import { AdComponent } from './ad/ad.component';
import { AuthComponent } from './auth/auth.component';
import { AdViewComponent } from './ad-view/ad-view.component';
import { EditAdComponent } from './edit-ad/edit-ad.component';
import { UserListComponent } from './user-list/user-list.component';
import { NewUserComponent } from './new-user/new-user.component';
import { SingleAdComponent } from './single-ad/single-ad.component';
import { FourOhFourComponent } from './four-oh-four/four-oh-four.component';

//Services
import { AdService } from './services/ad.service';
import { AuthService} from  './services/auth.service';
import { AuthGuard } from './services/auth-guard.service';
import { UserService} from './services/user.service';


const appRoutes : Routes = [
  { path: 'annonces', canActivate:[AuthGuard],component: AdViewComponent },
  { path: 'annonces/:id', canActivate:[AuthGuard],component: SingleAdComponent},
  { path: 'edit', canActivate:[AuthGuard], component: EditAdComponent},
  { path: 'auth', component: AuthComponent },
  { path: 'users', component: UserListComponent },
  { path: 'new-user', component: NewUserComponent},
  { path: '', component: AdViewComponent},
  { path: 'not-found', component: FourOhFourComponent},
  { path: '**', redirectTo: '/not-found'}
];

@NgModule({
  declarations: [
    AppComponent,
    AdComponent,
    AuthComponent,
    AdViewComponent,
    SingleAdComponent,
    FourOhFourComponent,
    EditAdComponent,
    UserListComponent,
    NewUserComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    AdService,
    AuthService,
    AuthGuard,
    UserService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
