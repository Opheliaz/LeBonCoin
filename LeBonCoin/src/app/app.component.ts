import { Component, OnInit, OnDestroy} from "@angular/core";
import { AdService } from "./services/ad.service";
import { Observable } from "rxjs/Observable";
import "rxjs/Rx";
import { Subscription } from "rxjs/Rx";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent implements OnInit,
OnDestroy {
  title = "le bon coin ";
  secondes: number;
  counterSubcription: Subscription;

  constructor() {}
  
  ngOnInit() {
    const counter = Observable.interval(1000);
    this.counterSubcription = counter.subscribe((value: number) => {
      this.secondes = value;
    });
  
    // counter.subscribe(
    //   (value: number)=>{
    //     this.secondes = value;
    //   },
    //   (error: any)=>{
    //     console.log('Une erreur a été rencontrée !');
    //   },
    //   ()=>{
    //     console.log('Observable complété!');
    //   }
    // );
  }
  ngOnDestroy(){
    this.counterSubcription.unsubscribe();
  }
}
