import { Subject} from 'rxjs/Subject';
// import { stat } from 'fs';
import{ HttpClient} from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class AdService {
  annoncesSubject = new Subject<any[]>();
  private annonces = [
    // {
    //   id: 1,
    //   name: "Moto à 6 roues",
    //   status: "Désactivée"
    // },
    // {
    //   id: 2,
    //   name: "Chien à 3 tête",
    //   status: "Publié"
    // },
    // {
    //   id: 3,
    //   name: "Valse à 1000 temps",
    //   status: "Désactivée"
    // }
  ];
  constructor(private httpClient : HttpClient ){}

  emitAdSubject(){
    this.annoncesSubject.next(
      this.annonces.slice());

  }

  getAdById( id: number){
    const annonce = this.annonces.find(
      (annonceObject) =>{
        return annonceObject.id === id;
      }
    );
    return annonce;
  }

  switchOnAll() {
    for (let annonce of this.annonces) {
        annonce.status = "Publié";
    }
    this.emitAdSubject();
  }
  switchOffAll(){
      for(let annonce of this.annonces){
          annonce.status = "Désactivée";
      }
      this.emitAdSubject();
  }
  switchOnOne(index: number){
      this.annonces[index].status = "Publié";
      this.emitAdSubject();
  }
  switchOffOne(index: number){
      this.annonces[index].status = "Désactivée"
      this.emitAdSubject();
  }

  AddAnnonce(name: string, status: string){
    //methode template
    const annonceObject = {
      id: 0,
      name:'',
      status:''
    };
    annonceObject.name = name;
    annonceObject.status = status;
    annonceObject.id = this.annonces[(this.annonces.length - 1)].id + 1 ;

    this.annonces.push(annonceObject);
    this.emitAdSubject();
  }

  saveAdToServer(){
    this.httpClient
    .put('https://http-client-demo-ae664.firebaseio.com/annonces.json', this.annonces)
    .subscribe(
      ()=>{
        console.log('Enregistrement terminé !');
      },
      (error)=>{
        console.log("Erreur de sauvegarde !" + error );
      }
    )
  }
  getAdFromServer(){
    this.httpClient
    .get<any[]>('https://http-client-demo-ae664.firebaseio.com/annonces.json')
    .subscribe(
      (response)=>{
        this.annonces = response;
        this.emitAdSubject();
      },
      (error) =>{
        console.log('Erreur de chargement !' + error);      }
    )
  }

}
