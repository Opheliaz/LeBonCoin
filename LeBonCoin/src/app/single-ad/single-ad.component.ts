import { Component, OnInit } from "@angular/core";
import { AdService } from "../services/ad.service";
import { ActivatedRoute} from "@angular/router";

@Component({
  selector: "app-single-ad",
  templateUrl: "./single-ad.component.html",
  styleUrls: ["./single-ad.component.scss"]
})
export class SingleAdComponent implements OnInit {
  name: string = "Annonce";
  status : string = 'Statut';

  constructor(private adService: AdService,
              private route : ActivatedRoute) {}

  ngOnInit() {
    const id = this.route.snapshot.params['id'];
    this.name = this.adService.getAdById(+id).name;
    this.status = this.adService.getAdById(+id).status;
  }
}
