import { Component, Input , OnInit } from '@angular/core';
import { AdService } from '../services/ad.service';

@Component({
  selector: 'app-ad',
  templateUrl: './ad.component.html',
  styleUrls: ['./ad.component.scss']
})

export class AdComponent implements OnInit {

  @Input()annonceName: string;
  @Input()annonceStatus: string;
  @Input()indexOfAd : number;
  @Input() id: number;
  
  constructor(private adService: AdService) { }

  ngOnInit() {
  }

  getStatus(){
    return this.annonceStatus;
  }
  getColor(){
    if(this.annonceStatus ==="Publié"){
      return 'green';
    } else if(this.annonceStatus === "Désactivée"){
      return 'red';
    }
  }
  onSwitchOn(){
    this.adService.switchOnOne(this.indexOfAd);
  }
  onSwitchOff(){
    this.adService.switchOffOne(this.indexOfAd);
  }

}

