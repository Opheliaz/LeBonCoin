import { Component, OnInit } from '@angular/core';
import { NgForm} from "@angular/forms";
import { AdService} from '../services/ad.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-edit-ad',
  templateUrl: './edit-ad.component.html',
  styleUrls: ['./edit-ad.component.scss']
})
export class EditAdComponent implements OnInit {

  defaultOnOff= 'Désactivée';
//methode template
  constructor(private adService: AdService,
  private router: Router){

  }

  ngOnInit() {
  }
  onSubmit(form: NgForm){
    // console.log(form.value);
    //methode template.
    const name =form.value['name'];
    const status = form.status['status'];
    this.adService.AddAnnonce(name, status);
    this.router.navigate(['/annonces']);

  }
}
