import { Component, OnInit } from '@angular/core';
import { AdService } from '../services/ad.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-ad-view',
  templateUrl: './ad-view.component.html',
  styleUrls: ['./ad-view.component.scss']
})
export class AdViewComponent implements OnInit {

 
  isAuth = false;

  lastUpDate = new Promise(
    (resolve, reject)=>{
      const date = new Date ();
      setTimeout(
        ()=>{
          resolve(date);
        }, 2000
      );
    });

  
  annonces: any[];
  annonceSubscription: Subscription;

  // adOne = "Moto à 6";
  // adTwo = "Chien à 3 têtes";
  // adThree = "Valse  à 1000 temps ";

  constructor(private adService: AdService){
    setTimeout(
      () =>{
        this.isAuth = true;
      }, 4000
    );
  }
  ngOnInit(){
    this.annonceSubscription = this.adService.annoncesSubject.
    subscribe((annonces : any[]) => {
      this.annonces = annonces;
    }
  );
  this.adService.emitAdSubject();
    // this.annonces = this.adService.annonces;
  }
  onPublier(){
    this.adService.switchOnAll();
  }
  onDesactiv(){
    this.adService.switchOffAll();
  }
  onSave(){
    this.adService.saveAdToServer();
  }

  onFetch(){
    this.adService.getAdFromServer();
  }

}
